module.exports = {
    getAll: async function (userId) {
        let [result, ignored] = await sql.query("SELECT * FROM mobile_tokens WHERE driver_id = ? limit 1", [userId]);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    getAllFirebase: async function () {
        let [result, ignored] = await sql.query("SELECT token_firebase FROM mobile_tokens");
        return result;
    },
    checkHaveAndroid: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_tokens WHERE driver_id = ? AND device = ? limit 1", [userId,'android']);
        return result;
    },
    checkHaveIos: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_tokens WHERE driver_id = ? AND device = ? limit 1", [userId,'ios']);
        return result;
    },
    getTokens: async function (userId) {
        let [result, ignored] = await sql.query("SELECT token_firebase FROM mobile_tokens WHERE driver_id = ?", [userId]);
        return result;
    },
    insert: async function (token) {
        let [result, ignored] = await sql.query("INSERT INTO mobile_tokens (device, driver_id, token_firebase) VALUES (?, ?,?)", [token.device, token.driver_id,token.token_firebase]);
        return true;
    },
    update: async function (token) {
        let [result, ignored] = await sql.query("UPDATE mobile_tokens SET device = ?, driver_id = ?, token_firebase = ?  WHERE id = ?", [token.device,  token.driver_id, token.token_firebase, token.id]);
        return true;
    }
};
