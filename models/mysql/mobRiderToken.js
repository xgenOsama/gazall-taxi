module.exports = {
    getAll: async function (userId) {
        let [result, ignored] = await sql.query("SELECT * FROM mobile_rider_tokens WHERE rider_id = ? limit 1", [userId]);
        //result.map(x=>x.location = [x.location.y,x.location.x]);
        return result;
    },
    getAllFirebase: async function () {
        let [result, ignored] = await sql.query("SELECT token_firebase FROM mobile_rider_tokens");
        return result;
    },
    checkHaveAndroid: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_rider_tokens WHERE rider_id = ? AND device = ? limit 1", [userId,'android']);
        return result;
    },
    checkHaveIos: async function(userId){
        let [result, ignored] = await sql.query("SELECT * FROM mobile_rider_tokens WHERE rider_id = ? AND device = ? limit 1", [userId,'ios']);
        return result;
    },
    getTokens: async function (userId) {
        let [result, ignored] = await sql.query("SELECT token_firebase FROM mobile_rider_tokens WHERE rider_id = ? ", [userId]);
        return result;
    },
    insert: async function (token) {
        let [result, ignored] = await sql.query("INSERT INTO mobile_rider_tokens (device,token_firebase, rider_id) VALUES (?, ?, ?)", [token.device, token.token_firebase, token.rider_id]);
        return true;
    },
    update: async function (token) {
        let [result, ignored] = await sql.query("UPDATE mobile_rider_tokens SET device = ?,token_firebase = ? , rider_id = ? WHERE id = ?", [token.device, token.token_firebase, token.rider_id, token.id]);
        return true;
    }
};