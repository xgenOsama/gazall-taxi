/** @namespace req.query.user_name */

const mysql = require('../models/mysql');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = new express.Router();
router.get('/', (req, res) => {
    res.status(200).send("Server App is running OK!").end();
});
router.post("/operator_login", async function (req, res) {
    try {
        let operator = (await mysql.operator.authenticate(req.query.user_name, req.query.password));
        let token = jwt.sign({id: operator.id}, jwtToken, {});
        mysql.operator.setStatus(operator.id,'enabled');
        res.json({status: 200, token: token, user: operator});
    }
    catch (err) {
        if (isNaN(err.message)) {
            res.json({status: 666, error: err.message});
        } else {
            res.json({status: err.message});
        }
    }
});
router.post('/rider_login', async function (req, res) {
    if (process.env.RIDER_MIN_VERSION && req.body.version && parseInt(req.body.version) < process.env.RIDER_MIN_VERSION) {
        res.json({status: 410});
        return;
    }
    let profile = await mysql.rider.getProfile(parseInt(req.body.user_name));
    ////////////////////////// token //////////////////////////////////////
        if(req.body.device == 'android'){
        checkHaveAndroidToken = await mysql.mobRiderToken.checkHaveAndroid(profile.id);
        if(checkHaveAndroidToken[0]){
            if(checkHaveAndroidToken[0].token_firebase != req.body.token_firebase){
                await mysql.mobRiderToken.update({
                    device:'android',
                    rider_id:profile.id,
                    token_firebase: req.body.token_firebase,
                    id: checkHaveAndroidToken[0].id
                });
            }
        }else{
            await mysql.mobRiderToken.insert({
                device:'android',
                rider_id:profile.id,
                token_firebase: req.body.token_firebase,
            });
        }
    }else if (req.body.device == 'ios'){
        checkHaveIosToken = await mysql.mobRiderToken.checkHaveIos(profile.id);
        if(checkHaveIosToken[0]){
            if( checkHaveIosToken[0].token_firebase != req.body.token_firebase){
                await mysql.mobRiderToken.update({
                    device:'ios',
                    rider_id:profile.id,
                    token_firebase: req.body.token_firebase,
                    id: checkHaveIosToken[0].id
                });
            }
        }else{
            await mysql.mobRiderToken.insert({
                device:'ios',
                rider_id:profile.id,
                token_firebase: req.body.token_firebase,
            });
        }
    }
    //////////////////////////////////////////////////////////////////////
    switch (profile.status) {
        case('blocked'):
            res.json({status: 412});
            return;
    }
    let keys = {
        id: profile.id,
        prefix: riderPrefix
    };
    let token = jwt.sign(keys, jwtToken, {});
    res.json({status: 200, token: token, user: profile});
});
router.post('/driver_login', async function (req, res) {
    if (process.env.DRIVER_MIN_VERSION && req.body.version && parseInt(req.body.version) < process.env.DRIVER_MIN_VERSION) {
        res.json({status: 410});
        return;
    }
    let profile = await mysql.driver.getProfile(parseInt(req.body.user_name));
    ////////////////////////// token //////////////////////////////////////
    if(req.body.device == 'android'){
        checkHaveAndroidToken = await mysql.mobToken.checkHaveAndroid(profile.id);
        if(checkHaveAndroidToken[0]){
            if( checkHaveAndroidToken[0].token_firebase != req.body.token_firebase ){
                mysql.mobToken.update({
                    device:'android',
                    driver_id:profile.id,
                    token_firebase: req.body.token_firebase,
                    id: checkHaveAndroidToken[0].id
                });
            }
        }else{
            mysql.mobToken.insert({
                device:'android',
                driver_id:profile.id,
                token_firebase: req.body.token_firebase
            });
        }
    }else if (req.body.device == 'ios'){
        checkHaveIosToken = await mysql.mobToken.checkHaveIos(profile.id);
        if(checkHaveIosToken[0]){
            if( checkHaveIosToken[0].token_firebase != req.body.token_firebase ){
                mysql.mobToken.update({
                    device:'ios',
                    driver_id:profile.id,
                    token_firebase: req.body.token_firebase,
                    id: checkHaveIosToken[0].id
                });
            }
        }else{
            mysql.mobToken.insert({
                device:'ios',
                driver_id:profile.id,
                token_firebase: req.body.token_firebase,
            });
        }
    }
    //////////////////////////////////////////////////////////////////////
    switch (profile.status) {
        case('disabled'):
            res.json({
                status: 411
            });
            return;
        case('blocked'):
            res.json({status: 412});
            return;
    }
    let keys = {
        id: profile.id,
        prefix: driverPrefix
    };
    let token = jwt.sign(keys, jwtToken, {});
    res.json({status: 200, token: token, user: profile});
});
module.exports = router;